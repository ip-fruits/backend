# Backend System Requirements
PHP version (CLI and FPM): 8.2.4
PostgreSql version: 15.2
Symfony binary optional (run symfony commands with ```php bin/console``` if you choose not to install it. Otherwise use ```symfony console```.)

# Instructions
- Run ```composer install```.
- Run ```symfony console doctrine:create:database```.
- Run ```symfony console doctrine:migrations:migrate```.
- Run ```symfony console doctrine:fixtures:load```.
- Run ```symfony server:start``` to serve the project locally.

# Important information
I tried creating a containerized version of the backend but I had significant issues with having the container find the pdo_pgsql driver even though the Flex recipes seemed to be updating the docker-compose.yml and Dockerfile properly. I spend a couple of hours trying to debug it until I eventually gave up and ran everything locally, apologies.

The email address that is being notified when fruit data is imported is my personal email. That setting can be found on line 56 at ```src/DataFixtures/FruitFixtures.php```. Due to the fact that this goes through my free Mailgun account I need to approve any recipients so even if you change it, you will not receive an email. I can demonstrate it live or approve your email so you can test it yourselves.