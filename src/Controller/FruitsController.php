<?php

namespace App\Controller;

use App\Entity\Fruit;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class FruitsController extends AbstractController
{
    private $em;
    private $serializer;

    public function __construct(EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->serializer = $serializer;
    }

    #[Route('/api/getFruits', name: 'get_fruits')]
    public function index(): JsonResponse
    {
        $fruitRepository = $this->em->getRepository(Fruit::class);
        $fruits = $fruitRepository->findAll();

        $serializedFruits = $this->serializer->serialize($fruits, 'json');

        return new JsonResponse($serializedFruits, 200, [], true);
    }
}