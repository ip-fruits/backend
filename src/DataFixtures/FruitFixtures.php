<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpClient\HttpClient;
use APp\Entity\Fruit;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\HttpFoundation\Response;

class FruitFixtures extends Fixture
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function load(ObjectManager $manager): void
    {
        //Make the GET request to FruityVice
        $client = HttpClient::create();
        $response = $client->request('GET', 'https://fruityvice.com/api/fruit/all');

        $statusCode = $response->getStatusCode();
        //Stop the function if the GET request is not successful
        if ($statusCode !== 200) {
            return;
        }
        $fruits = $response->toArray();
        
        //Loop the response and create entity objects to persist to the database
        foreach ($fruits as $fruit) {
            $fruitObject = new Fruit();
            $fruitObject->setGenus($fruit['genus']);
            $fruitObject->setName($fruit['name']);
            $fruitObject->setFamily($fruit['family']);
            $fruitObject->setFruitOrder($fruit['order']);
            $fruitObject->setCarbohydrates($fruit['nutritions']['carbohydrates']);
            $fruitObject->setProtein($fruit['nutritions']['protein']);
            $fruitObject->setFat($fruit['nutritions']['fat']);
            $fruitObject->setCalories($fruit['nutritions']['calories']);
            $fruitObject->setSugar($fruit['nutritions']['sugar']);
            $manager->persist($fruitObject);
        }
        $this->sendEmail($this->mailer);
        $manager->flush();
    }

    private function sendEmail(MailerInterface $mailer) {
        $email = (new Email())
            ->from('fruits@test.com')
            ->to('elias.papa92@gmail.com')
            ->subject('Fruit Database Import')
            ->text('Fruits have been imported from FruitVice!');

        try {
            $mailer->send($email);
            return new Response(
              'Email was sent.'
            );
        } catch (TransportExceptionInterface $e) {
            return $e;
        }
    }
}
