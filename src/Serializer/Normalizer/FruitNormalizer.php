<?php

namespace App\Serializer\Normalizer;

use App\Entity\Fruit;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class FruitNormalizer implements NormalizerInterface
{
    public function normalize($object, string $format = null, array $context = [])
    {
        if (!$object instanceof Fruit) {
            throw new \InvalidArgumentException('The object must be an instance of ' . Fruit::class);
        }

        return [
            'id' => $object->getId(),
            'name' => $object->getName(),
            'genus' => $object->getGenus(),
            'family' => $object->getFamily(),
            'fruit_order' => $object->getFruitOrder(),
            'carbohydrates' => $object->getCarbohydrates(),
            'protein' => $object->getProtein(),
            'fat' => $object->getFat(),
            'calories' => $object->getCalories(),
            'sugar' => $object->getSugar(),
        ];
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof Fruit;
    }
}